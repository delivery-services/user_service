package jwt

import (
	"errors"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

func GenerateJWT(m map[string]interface{}, tokenExpireTime time.Duration, tokenSecretKey string) (tokenString string, err error) {
	var (
		token *jwt.Token
	)

	token = jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	for key, value := range m {
		claims[key] = value
	}

	claims["iat"] = time.Now().Unix()
	claims["exp"] = time.Now().Add(tokenExpireTime).Unix()

	tokenString, err = token.SignedString([]byte(tokenSecretKey))
	if err != nil {
		return "", nil
	}

	return tokenString, nil
}

func ExtractClaims(tokenString string, tokenSecretKey string) (jwt.MapClaims, error) {

	var (
		token *jwt.Token
		err   error
	)

	token, err = jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(tokenSecretKey), nil
	})

	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !(ok && token.Valid) {
		return nil, errors.New("INVALID TOKEN")
	}
 
	return claims, nil
}

func ExtractToken(bearer string) (token string, err error) {
	strArr := strings.Split(bearer, " ")
	 if len(strArr) == 2 {
		return strArr[1], nil
	 }

	 return token, errors.New("WRONG TOKEN FORMAT")

}