package client

import (
	"user_service/config"
	"user_service/genproto/delivery-protos/user_service"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	UserService() user_service.UserServiceClient
	ClientService() user_service.ClientServiceClient
	CourierService() user_service.CourierServiceClient
	BranchService() user_service.BranchServiceClient
}

type grpcClients struct {
	userService    user_service.UserServiceClient
	clientService  user_service.ClientServiceClient
	courierService user_service.CourierServiceClient
	branchService  user_service.BranchServiceClient
	authService    user_service.AuthServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connUserService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService:    user_service.NewUserServiceClient(connUserService),
		branchService:  user_service.NewBranchServiceClient(connUserService),
		clientService:  user_service.NewClientServiceClient(connUserService),
		courierService: user_service.NewCourierServiceClient(connUserService),
		authService:    user_service.NewAuthServiceClient(connUserService),
	}, nil

}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}

func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}

func (g *grpcClients) ClientService() user_service.ClientServiceClient {
	return g.clientService
}

func (g *grpcClients) CourierService() user_service.CourierServiceClient {
	return g.courierService
}

func (g *grpcClients) AuthService() user_service.AuthServiceClient {
	return g.authService
}
