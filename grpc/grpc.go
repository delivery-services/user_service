package grpc

import (
	pb "user_service/genproto/delivery-protos/user_service"
	"user_service/grpc/client"
	"user_service/service"
	"user_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(storage storage.IStorage, services client.IServiceManager) *grpc.Server {
	grpcServer := grpc.NewServer()

	pb.RegisterUserServiceServer(grpcServer, service.NewUserService(storage, services))
	pb.RegisterBranchServiceServer(grpcServer, service.NewBranchService(storage, services))
	pb.RegisterClientServiceServer(grpcServer, service.NewClientService(storage, services))
	pb.RegisterCourierServiceServer(grpcServer, service.NewCourierService(storage, services))
	pb.RegisterAuthServiceServer(grpcServer, service.NewAuthService(storage, services))

	reflection.Register(grpcServer)

	return grpcServer

}
