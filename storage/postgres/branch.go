package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	pb "user_service/genproto/delivery-protos/user_service"
	"user_service/pkg/logger"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type branchRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	pb.UnimplementedBranchServiceServer
}

func NewBranchRepo(db *pgxpool.Pool, log logger.ILogger) *branchRepo {
	return &branchRepo{
		db:  db,
		log: log,
	}
}

func (b *branchRepo) Create(ctx context.Context, branch *pb.CreateBranch) (*pb.Branch, error) {

	var response = pb.Branch{}

	id := uuid.New()

	query := `insert into branch (
		id, 
		name,
		phone,
		photo,
		delivery_tarif_id,
		working_hour,
		address,
		destination, 
		active
	 ) values ($1, $2, $3, $4, $5, $6, $7, $8, $9)
	   returning
	    id, 
		name,
		phone,
		photo,
		delivery_tarif_id,
		working_hour,
		address,
		destination, 
		active
	 `

	if err := b.db.QueryRow(ctx, query,
		id,
		branch.Name,
		branch.Phone,
		branch.Photo,
		branch.DeliveryTarifId,
		branch.WorkingHour,
		branch.Address,
		branch.Destination,
		branch.Active,
	).Scan(
		&response.Id,
		&response.Name,
		&response.Phone,
		&response.Photo,
		&response.DeliveryTarifId,
		&response.WorkingHour,
		&response.Address,
		&response.Destination,
		&response.Active,
	); err != nil {
		b.log.Error("error in branch service while inserting and getting branch", logger.Error(err))
		return nil, err
	}

	return &response, nil

}

func (b *branchRepo) GetByID(ctx context.Context, pKey *pb.GetBranchByID) (*pb.Branch, error) {

	var updatedAt = sql.NullString{}

	branch := &pb.Branch{}

	query := `select 
	 id,
	 name, 
	 phone, 
	 photo, 
	 delivery_tarif_id, 
	 working_hour, 
	 address, 
	 destination, 
	 active,  
	 created_at::text, 
	 updated_at::text 
	 from branch where deleted_at = 0 and id = $1`

	row := b.db.QueryRow(ctx, query, pKey.Id)

	err := row.Scan(
		&branch.Id,
		&branch.Name,
		&branch.Phone,
		&branch.Photo,
		&branch.DeliveryTarifId,
		&branch.WorkingHour,
		&branch.Address,
		&branch.Destination,
		&branch.Active,
		&branch.CreatedAt,
		&updatedAt,
	)

	if err != nil {
		log.Println("error while selecting branch", err.Error())
		return &pb.Branch{}, err
	}

	if updatedAt.Valid {
		branch.UpdatedAt = updatedAt.String
	}

	return branch, nil

}

func (b *branchRepo) GetAll(ctx context.Context, request *pb.GetBranchList) (*pb.BranchesResponse, error) {
	var (
		branches          = []*pb.Branch{}
		count             = 0
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
		updatedAt         = sql.NullString{}
	)

	countQuery = `select count(1) from branch where deleted_at = 0 `

	if search != "" {
		countQuery += fmt.Sprintf(` and name ilike '%%%s%%' or address ilike '%%%s%%'`, search, search)
	}
	if err := b.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		b.log.Error("error is while selecting count", logger.Error(err))
		return &pb.BranchesResponse{}, err
	}

	query = `select 
	 id,
	 name, 
	 phone, 
	 photo, 
	 delivery_tarif_id, 
	 working_hour, 
	 address, 
	 destination, 
	 active,  
	 created_at::text, 
	 updated_at::text 
	 from branch where deleted_at = 0 `

	if search != "" {
		query += fmt.Sprintf(` and name ilike '%%%s%%' or address ilike '%%%s%%'`, search, search)
	}

	query += ` order by created_at desc LIMIT $1 OFFSET $2`
	rows, err := b.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		b.log.Error("error is while selecting branch", logger.Error(err))
		return &pb.BranchesResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		branch := pb.Branch{}
		if err = rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.Photo,
			&branch.DeliveryTarifId,
			&branch.WorkingHour,
			&branch.Address,
			&branch.Destination,
			&branch.Active,
			&branch.CreatedAt,
			&updatedAt); err != nil {
			b.log.Error("error is while scanning branch data", logger.Error(err))
			return &pb.BranchesResponse{}, err
		}

		if updatedAt.Valid {
			branch.UpdatedAt = updatedAt.String
		}

		branches = append(branches, &branch)

	}

	if err := rows.Err(); err != nil {
		b.log.Error("error while iterating rows", logger.Error(err))
		return &pb.BranchesResponse{}, err
	}

	return &pb.BranchesResponse{
		Branch: branches,
		Count:  int32(count),
	}, nil
}

func (b *branchRepo) Update(ctx context.Context, request *pb.UpdateBranch) (*pb.BranchResponse, error) {
	query := `
		update branch set 
		name = $1, 
		phone = $2,
        photo = $3,
		delivery_tarif_id = $4,
		working_hour = $5,
		address = $6,
		destination = $7,
		active= $8,
		updated_at = now()
		where id = $9`

	if _, err := b.db.Exec(ctx, query,
		request.Name,
		request.Phone,
		request.Photo,
		request.DeliveryTarifId,
		request.WorkingHour,
		request.Address,
		request.Destination,
		request.Active,
		request.Id); err != nil {
		fmt.Println("error while updating branch data", err.Error())
		return &pb.BranchResponse{}, err
	}

	return &pb.BranchResponse{
		Response: "data succesfully updated",
	}, nil
}

func (b *branchRepo) Delete(ctx context.Context, request *pb.GetBranchByID) (*pb.BranchResponse, error) {

	query := `update branch set deleted_at = extract(epoch from current_timestamp) where id = $1`

	if _, err := b.db.Exec(ctx, query, request.Id); err != nil {
		fmt.Println("error while deleting branch by id", err.Error())
		return nil, err
	}

	return &pb.BranchResponse{
		Response: "data succesfully deleted",
	}, nil
}


func (b *branchRepo) GetActiveBranches(ctx context.Context, request *pb.GetBranchList) (*pb.BranchesResponse, error) {
	var (
		branches          = []*pb.Branch{}
		count             = 0
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
		updatedAt         = sql.NullString{}
	)

	countQuery = `select count(1) from branch where deleted_at = 0 and working_hour = 'start' and active = 'active'`

	if search != "" {
		countQuery += fmt.Sprintf(` and name ilike '%%%s%%' or address ilike '%%%s%%'`, search, search)
	}
	if err := b.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		b.log.Error("error is while selecting count", logger.Error(err))
		return &pb.BranchesResponse{}, err
	}

	query = `select 
	 id,
	 name, 
	 phone, 
	 photo, 
	 delivery_tarif_id, 
	 working_hour, 
	 address, 
	 destination, 
	 active,  
	 created_at::text, 
	 updated_at::text 
	 from branch where deleted_at = 0 and working_hour = 'start' and active = 'active'`

	if search != "" {
		query += fmt.Sprintf(` and name ilike '%%%s%%' or address ilike '%%%s%%'`, search, search)
	}

	query += ` order by created_at desc LIMIT $1 OFFSET $2`
	rows, err := b.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		b.log.Error("error is while selecting branch", logger.Error(err))
		return &pb.BranchesResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		branch := pb.Branch{}
		if err = rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.Photo,
			&branch.DeliveryTarifId,
			&branch.WorkingHour,
			&branch.Address,
			&branch.Destination,
			&branch.Active,
			&branch.CreatedAt,
			&updatedAt); err != nil {
			b.log.Error("error is while scanning branch data", logger.Error(err))
			return &pb.BranchesResponse{}, err
		}

		if updatedAt.Valid {
			branch.UpdatedAt = updatedAt.String
		}

		branches = append(branches, &branch)

	}

	if err := rows.Err(); err != nil {
		b.log.Error("error while iterating rows", logger.Error(err))
		return &pb.BranchesResponse{}, err
	}

	return &pb.BranchesResponse{
		Branch: branches,
		Count:  int32(count),
	}, nil
}

