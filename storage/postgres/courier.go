package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	pb "user_service/genproto/delivery-protos/user_service"
	"user_service/pkg/check"
	"user_service/pkg/logger"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type courierRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	pb.UnimplementedCourierServiceServer
}

func NewCourierRepo(db *pgxpool.Pool, log logger.ILogger) *courierRepo {
	return &courierRepo{
		db:  db,
		log: log,
	}
}

func (c *courierRepo) Create(ctx context.Context, courier *pb.CreateCourier) (*pb.Courier, error) {
	var response = pb.Courier{}

	id := uuid.New()

	query := `INSERT INTO courier (
		id, 
		first_name,
		last_name,
		phone,
		date_of_birth,
		age,
		gender,
		active,
		login,
		password,
		max_order_count,
		branch_id
	) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
	   RETURNING
		id, 
		first_name,
		last_name,
		phone,
		date_of_birth,
		age,
		gender,
		active,
		login,
		password,
		max_order_count,
		branch_id
	`

	if err := c.db.QueryRow(ctx, query,
		id,
		courier.FirstName,
		courier.LastName,
		courier.Phone,
		courier.DateOfBirth,
		check.CalculateAge(courier.DateOfBirth),
		courier.Gender,
		courier.Active,
		courier.Login,
		courier.Password,
		courier.MaxOrderCount,
		courier.BranchId,
	).Scan(
		&response.Id,
		&response.FirstName,
		&response.LastName,
		&response.Phone,
		&response.DateOfBirth,
		&response.Age,
		&response.Gender,
		&response.Active,
		&response.Login,
		&response.Password,
		&response.MaxOrderCount,
		&response.BranchId,
	); err != nil {
		c.log.Error("error in courier service while inserting and getting courier")
		return nil, err
	}

	return &response, nil
}

func (c *courierRepo) GetByID(ctx context.Context, pKey *pb.GetCourierByID) (*pb.Courier, error) {
	var updatedAt = sql.NullString{}
	courier := &pb.Courier{}

	query := `SELECT 
		id, 
		first_name,
		last_name,
		phone,
		date_of_birth,
		age,
		gender,
		active,
		login,
		password,
		max_order_count,
		branch_id,
		created_at::text, 
		updated_at::text 
	FROM courier 
	WHERE deleted_at = 0 AND id = $1`

	row := c.db.QueryRow(ctx, query, pKey.Id)

	err := row.Scan(
		&courier.Id,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.DateOfBirth,
		&courier.Age,
		&courier.Gender,
		&courier.Active,
		&courier.Login,
		&courier.Password,
		&courier.MaxOrderCount,
		&courier.BranchId,
		&courier.CreatedAt,
		&updatedAt,
	)

	if err != nil {
		log.Println("error while selecting courier", err.Error())
		return &pb.Courier{}, err
	}

	if updatedAt.Valid {
		courier.UpdatedAt = updatedAt.String
	}

	return courier, nil
}

func (c *courierRepo) GetAll(ctx context.Context, request *pb.GetCourierList) (*pb.CouriersResponse, error) {
	var (
		couriers          = []*pb.Courier{}
		count             = 0
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
		updatedAt         = sql.NullString{}
	)

	countQuery = `SELECT COUNT(1) FROM courier WHERE deleted_at = 0 `

	if search != "" {
		countQuery += fmt.Sprintf(` AND (first_name ILIKE '%%%s%%' OR last_name ILIKE '%%%s%%')`, search, search)
	}

	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		c.log.Error("error while selecting courier count", logger.Error(err))
		return &pb.CouriersResponse{}, err
	}

	query = `SELECT 
		id, 
		first_name,
		last_name,
		phone,
		date_of_birth,
		age,
		gender,
		active,
		login,
		password,
		max_order_count,
		branch_id,
		created_at::text, 
		updated_at::text 
	FROM courier 
	WHERE deleted_at = 0 `

	if search != "" {
		query += fmt.Sprintf(` AND (first_name ILIKE '%%%s%%' OR last_name ILIKE '%%%s%%')`, search, search)
	}

	query += ` ORDER BY created_at DESC LIMIT $1 OFFSET $2`

	rows, err := c.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		c.log.Error("error while selecting courier", logger.Error(err))
		return &pb.CouriersResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		courier := pb.Courier{}
		if err = rows.Scan(
			&courier.Id,
			&courier.FirstName,
			&courier.LastName,
			&courier.Phone,
			&courier.DateOfBirth,
			&courier.Age,
			&courier.Gender,
			&courier.Active,
			&courier.Login,
			&courier.Password,
			&courier.MaxOrderCount,
			&courier.BranchId,
			&courier.CreatedAt,
			&updatedAt,
		); err != nil {
			c.log.Error("error while scanning courier data", logger.Error(err))
			return &pb.CouriersResponse{}, err
		}

		if updatedAt.Valid {
			courier.UpdatedAt = updatedAt.String
		}

		couriers = append(couriers, &courier)
	}

	if err := rows.Err(); err != nil {
		c.log.Error("error while iterating rows", logger.Error(err))
		return &pb.CouriersResponse{}, err
	}

	return &pb.CouriersResponse{
		Courier: couriers,
		Count:   int32(count),
	}, nil
}

func (c *courierRepo) Update(ctx context.Context, request *pb.UpdateCourier) (*pb.CourierResponse, error) {
	query := `
		UPDATE courier SET 
		first_name = $1, 
		last_name = $2,
        phone = $3,
		date_of_birth = $4,
		gender = $5,
		active = $6,
		login = $7,
		password = $8,
		max_order_count = $9,
		branch_id = $10,
		updated_at = NOW()
		WHERE id = $11`

	if _, err := c.db.Exec(ctx, query,
		request.FirstName,
		request.LastName,
		request.Phone,
		request.DateOfBirth,
		request.Gender,
		request.Active,
		request.Login,
		request.Password,
		request.MaxOrderCount,
		request.BranchId,
		request.Id,
	); err != nil {
		fmt.Println("error while updating courier data", err.Error())
		return &pb.CourierResponse{}, err
	}

	return &pb.CourierResponse{
		Response: "data successfully updated",
	}, nil
}

func (c *courierRepo) Delete(ctx context.Context, request *pb.GetCourierByID) (*pb.CourierResponse, error) {
	query := `UPDATE courier SET deleted_at = EXTRACT(EPOCH FROM CURRENT_TIMESTAMP) WHERE id = $1`

	if _, err := c.db.Exec(ctx, query, request.Id); err != nil {
		fmt.Println("error while deleting courier by id", err.Error())
		return nil, err
	}

	return &pb.CourierResponse{
		Response: "data successfully deleted",
	}, nil
}
