package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	pb "user_service/genproto/delivery-protos/user_service"
	"user_service/pkg/check"
	"user_service/pkg/logger"
	"user_service/pkg/security"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type userRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	pb.UnimplementedUserServiceServer
}

func NewUserRepo(db *pgxpool.Pool, log logger.ILogger) *userRepo {
	return &userRepo{
		db:  db,
		log: log,
	}
}

func (u *userRepo) Create(ctx context.Context, user *pb.CreateUser) (*pb.User, error) {

	var resp = pb.User{}

	id := uuid.New()

	query := `insert into users (
		id, 
		first_name,
		last_name,
		phone,
		date_of_birth,
		age,
		gender,
		active, 
		login,
		password
	 ) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
	   returning
	    id, 
		first_name,
		last_name,
		phone,
		date_of_birth,
		age,
		gender,
		active, 
		login,
		password
	 `

	hashPassword, err := security.HashPassword(user.Password)
	if err != nil {
		u.log.Error("error in user service while hashing user password")
		return nil, err
	}

	if err := u.db.QueryRow(ctx, query,
		id,
		user.FirstName,
		user.LastName,
		user.Phone,
		user.DateOfBirth,
		check.CalculateAge(user.DateOfBirth),
		user.Gender,
		user.Active,
		user.Login,
		hashPassword,
	).Scan(
		&resp.Id,
		&resp.FirstName,
		&resp.LastName,
		&resp.Phone,
		&resp.DateOfBirth,
		&resp.Age,
		&resp.Gender,
		&resp.Active,
		&resp.Login,
		&resp.Password,
	); err != nil {
		u.log.Error("error in user service while inserting and getting user")
		return nil, err
	}

	return &resp, nil

}

func (u *userRepo) GetByID(ctx context.Context, pKey *pb.GetUserByID) (*pb.User, error) {

	var updatedAt = sql.NullString{}

	user := &pb.User{}

	query := `select 
	 id,
	 first_name, 
	 last_name, 
	 phone, 
	 date_of_birth::text, 
	 age, 
	 gender, 
	 active, 
	 login, 
	 password, 
	 created_at::text, 
	 updated_at::text 
	 from users where deleted_at = 0 and id = $1`

	row := u.db.QueryRow(ctx, query, pKey.Id)

	err := row.Scan(
		&user.Id,
		&user.FirstName,
		&user.LastName,
		&user.Phone,
		&user.DateOfBirth,
		&user.Age,
		&user.Gender,
		&user.Active,
		&user.Login,
		&user.Password,
		&user.CreatedAt,
		&updatedAt,
	)

	if err != nil {
		log.Println("error while selecting user", err.Error())
		return &pb.User{}, err
	}

	if updatedAt.Valid {
		user.UpdatedAt = updatedAt.String
	}

	return user, nil

}

func (u *userRepo) GetAll(ctx context.Context, request *pb.GetUserList) (*pb.UsersResponse, error) {
	var (
		users             = []*pb.User{}
		count             = 0
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
		updatedAt         = sql.NullString{}
	)

	countQuery = `select count(1) from users where deleted_at = 0 `

	if search != "" {
		countQuery += fmt.Sprintf(` and first_name ilike '%%%s%%' or last_name ilike '%%%s%%'`, search, search)
	}
	if err := u.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		u.log.Error("error is while selecting count", logger.Error(err))
		return &pb.UsersResponse{}, err
	}

	query = `select 
	id, 
	first_name,
	last_name,
	phone,
	date_of_birth,
	age,
	gender,
	active,
	login,
	password,
    created_at::text, 
	updated_at::text
	from users where deleted_at = 0 `

	if search != "" {
		query += fmt.Sprintf(` and first_name ilike '%%%s%%' or last_name ilike '%%%s%%'`, search, search)
	}

	query += ` order by created_at desc LIMIT $1 OFFSET $2`
	rows, err := u.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		u.log.Error("error is while selecting user", logger.Error(err))
		return &pb.UsersResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		user := pb.User{}
		if err = rows.Scan(
			&user.Id,
			&user.FirstName,
			&user.LastName,
			&user.Phone,
			&user.DateOfBirth,
			&user.Age,
			&user.Gender,
			&user.Active,
			&user.Login,
			&user.Password,
			&user.CreatedAt,
			&updatedAt); err != nil {
			u.log.Error("error is while scanning user data", logger.Error(err))
			return &pb.UsersResponse{}, err
		}

		if updatedAt.Valid {
			user.UpdatedAt = updatedAt.String
		}

		users = append(users, &user)

	}

	if err := rows.Err(); err != nil {
		u.log.Error("error while iterating rows", logger.Error(err))
		return &pb.UsersResponse{}, err
	}

	return &pb.UsersResponse{
		User:  users,
		Count: int32(count),
	}, nil
}

func (u *userRepo) Update(ctx context.Context, request *pb.UpdateUser) (*pb.UserResponse, error) {
	query := `
		update users set 
		first_name = $1, 
		last_name = $2,
        phone = $3,
		date_of_birth = $4,
		gender = $5,
		active = $6,
		login = $7,
		updated_at = now()
				where id = $8`

	if _, err := u.db.Exec(ctx, query,
		request.FirstName,
		request.LastName,
		request.Phone,
		request.DateOfBirth,
		request.Gender,
		request.Active,
		request.Login,
		request.Id); err != nil {
		fmt.Println("error while updating user data", err.Error())
		return &pb.UserResponse{}, err
	}

	return &pb.UserResponse{
		Response: "data succesfully updated",
	}, nil
}

func (u *userRepo) Delete(ctx context.Context, request *pb.GetUserByID) (*pb.UserResponse, error) {

	query := `update users set deleted_at = extract(epoch from current_timestamp) where id = $1`

	if _, err := u.db.Exec(ctx, query, request.Id); err != nil {
		fmt.Println("error while deleting user by id", err.Error())
		return nil, err
	}

	return &pb.UserResponse{
		Response: "data succesfully deleted",
	}, nil
}

func (u *userRepo) GetPassword(ctx context.Context, login string) (string, error) {

	user := &pb.User{}

	query := `
		select password from users
	            where login = $1`

	if err := u.db.QueryRow(ctx, query, login).Scan(&user.Password); err != nil {
		fmt.Println("Error while scanning password from users", err.Error())
		return "", err
	}
	fmt.Println()
	fmt.Println(user.Password)
	fmt.Println(login)
	return user.Password, nil
}

func (u *userRepo) UpdatePassword(ctx context.Context, request *pb.UpdateUserPasswordRequest) (*pb.UserResponse, error) {

	query := `
		update users 
				set password = $1, updated_at = now()
					where login = $2`

	password, err := security.HashPassword(request.NewPassword)
	if err != nil {
		fmt.Println("error while hashed new password")
	}

	if _, err := u.db.Exec(ctx, query, password, request.Login); err != nil {
		fmt.Println("error while updating password for user", err.Error())
		return nil, err
	}

	return &pb.UserResponse{
		Response: "password succesfully updated",
	}, nil
}

func (c *userRepo) GetByCredentials(ctx context.Context, req *pb.UserLoginRequest) (*pb.User, error) {
	user := pb.User{}

	if err := c.db.QueryRow(ctx, `select id, password from users where login = $1`, req.GetLogin()).Scan(
		&user.Id,
		&user.Password,
	); err != nil {
		return nil, err
	}

	return &user, nil
}
