package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"
	pb "user_service/genproto/delivery-protos/user_service"
	"user_service/pkg/check"
	"user_service/pkg/logger"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type clientRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
	pb.UnimplementedClientServiceServer
}

func NewClientRepo(db *pgxpool.Pool, log logger.ILogger) *clientRepo {
	return &clientRepo{
		db:  db,
		log: log,
	}
}

func (c *clientRepo) Create(ctx context.Context, client *pb.CreateClient) (*pb.Client, error) {

	var response = pb.Client{}

	id := uuid.New()

	query := `insert into client (
		id, 
		first_name,
		last_name,
		phone,
		photo,
		date_of_birth,
		age,
		gender,
		last_ordered_date, 
		total_order_sum,
		total_order_count,
		discount_type,
		discount_amount
	 ) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
	   returning
	    id, 
		first_name,
		last_name,
		phone,
		photo,
		date_of_birth,
		age,
		gender,
		last_ordered_date, 
		total_order_sum,
		total_order_count,
		discount_type,
		discount_amount
	 `

	if err := c.db.QueryRow(ctx, query,
		id,
		client.FirstName,
		client.LastName,
		client.Phone,
		client.Photo,
		client.DateOfBirth,
		check.CalculateAge(client.DateOfBirth),
		client.Gender,
		client.LastOrderedDate,
		client.TotalOrdersSum,
		client.TotalOrdersCount,
		client.DiscountType,
		client.DiscountAmount,
	).Scan(
		&response.Id,
		&response.FirstName,
		&response.LastName,
		&response.Phone,
		&response.Photo,
		&response.DateOfBirth,
		&response.Age,
		&response.Gender,
		&response.LastOrderedDate,
		&response.TotalOrdersSum,
		&response.TotalOrdersCount,
		&response.DiscountType,
		&response.DiscountAmount,
	); err != nil {
		c.log.Error("error in user service while inserting and getting client")
		return nil, err
	}

	return &response, nil

}

func (c *clientRepo) GetByID(ctx context.Context, pKey *pb.GetClientByID) (*pb.Client, error) {

	var updatedAt = sql.NullString{}

	client := &pb.Client{}

	query := `select 
	id, 
	first_name,
	last_name,
	phone,
	photo,
	date_of_birth,
	age,
	gender,
	last_ordered_date, 
	total_order_sum,
	total_order_count,
	discount_type,
	discount_amount,
	created_at::text, 
	updated_at::text 
	 from client where deleted_at = 0 and id = $1`

	row := c.db.QueryRow(ctx, query, pKey.Id)

	err := row.Scan(
		&client.Id,
		&client.FirstName,
		&client.LastName,
		&client.Phone,
		&client.Photo,
		&client.DateOfBirth,
		&client.Age,
		&client.Gender,
		&client.LastOrderedDate,
		&client.TotalOrdersSum,
		&client.TotalOrdersCount,
		&client.DiscountType,
		&client.DiscountAmount,
		&client.CreatedAt,
		&updatedAt,
	)

	if err != nil {
		log.Println("error while selecting client", err.Error())
		return &pb.Client{}, err
	}

	if updatedAt.Valid {
		client.UpdatedAt = updatedAt.String
	}

	return client, nil

}

func (c *clientRepo) GetAll(ctx context.Context, request *pb.GetClientList) (*pb.ClientsResponse, error) {
	var (
		clients           = []*pb.Client{}
		count             = 0
		query, countQuery string
		page              = request.Page
		offset            = (page - 1) * request.Limit
		search            = request.Search
		updatedAt         = sql.NullString{}
	)

	countQuery = `select count(1) from client where deleted_at = 0 `

	if search != "" {
		countQuery += fmt.Sprintf(` and first_name ilike '%%%s%%' or last_name ilike '%%%s%%'`, search, search)
	}
	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		c.log.Error("error is while selecting client count", logger.Error(err))
		return &pb.ClientsResponse{}, err
	}

	query = `select 
	id, 
	first_name,
	last_name,
	phone,
	photo,
	date_of_birth,
	age,
	gender,
	last_ordered_date, 
	total_order_sum,
	total_order_count,
	discount_type,
	discount_amount,
	created_at::text, 
	updated_at::text 
	from client where deleted_at = 0 `

	if search != "" {
		query += fmt.Sprintf(` and first_name ilike '%%%s%%' or last_name ilike '%%%s%%'`, search, search)
	}

	query += ` order by created_at desc LIMIT $1 OFFSET $2`
	rows, err := c.db.Query(ctx, query, request.Limit, offset)
	if err != nil {
		c.log.Error("error is while selecting user", logger.Error(err))
		return &pb.ClientsResponse{}, err
	}

	defer rows.Close()

	for rows.Next() {
		client := pb.Client{}
		if err = rows.Scan(
			&client.Id,
			&client.FirstName,
			&client.LastName,
			&client.Phone,
			&client.Photo,
			&client.DateOfBirth,
			&client.Age,
			&client.Gender,
			&client.LastOrderedDate,
			&client.TotalOrdersSum,
			&client.TotalOrdersCount,
			&client.DiscountType,
			&client.DiscountAmount,
			&client.CreatedAt,
			&updatedAt); err != nil {
			c.log.Error("error is while scanning client data", logger.Error(err))
			return &pb.ClientsResponse{}, err
		}

		if updatedAt.Valid {
			client.UpdatedAt = updatedAt.String
		}

		clients = append(clients, &client)

	}

	if err := rows.Err(); err != nil {
		c.log.Error("error while iterating rows", logger.Error(err))
		return &pb.ClientsResponse{}, err
	}

	return &pb.ClientsResponse{
		Client: clients,
		Count:  int32(count),
	}, nil
}

func (c *clientRepo) Update(ctx context.Context, request *pb.UpdateClient) (*pb.ClientResponse, error) {
	query := `
		update client set 
		first_name = $1, 
		last_name = $2,
        phone = $3,
		photo = $4,
		date_of_birth = $5,
		gender = $6,
		last_ordered_date = $7,
		total_order_sum = $8,
		total_order_count = $9,
		discount_type = $10,
		discount_amount = $11,
		updated_at = now()
				where id = $12`

	if _, err := c.db.Exec(ctx, query,
		request.FirstName,
		request.LastName,
		request.Phone,
		request.Photo,
		request.DateOfBirth,
		request.Gender,
		request.LastOrderedDate,
		request.TotalOrdersSum,
		request.TotalOrdersCount,
		request.DiscountType,
		request.DiscountAmount,
		request.Id); err != nil {
		fmt.Println("error while updating client data", err.Error())
		return &pb.ClientResponse{}, err
	}

	return &pb.ClientResponse{
		Response: "data succesfully updated",
	}, nil
}

func (c *clientRepo) Delete(ctx context.Context, request *pb.GetClientByID) (*pb.ClientResponse, error) {

	query := `update client set deleted_at = extract(epoch from current_timestamp) where id = $1`

	if _, err := c.db.Exec(ctx, query, request.Id); err != nil {
		fmt.Println("error while deleting client by id", err.Error())
		return nil, err
	}

	return &pb.ClientResponse{
		Response: "data succesfully deleted",
	}, nil
}

func (c *clientRepo) UpdateClientData(ctx context.Context, request *pb.UpdateClientData) (*pb.ClientResponse, error) {
	query := `
		update client set 
		last_ordered_date = $1,
		total_order_sum =total_order_sum + $2,
		total_order_count = total_order_count + $3,
		updated_at = $4
				where id = $5`

	if _, err := c.db.Exec(ctx, query,
		request.LastOrderedDate,
		request.TotalOrdersSum,
		request.TotalOrdersCount,
        time.Now(),
		request.Id); err != nil {
		fmt.Println("error while updating client some  data", err.Error())
		return &pb.ClientResponse{}, err
	}

	return &pb.ClientResponse{
		Response: "data succesfully updated",
	}, nil
}
