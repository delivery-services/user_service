package storage

import (
	"context"
	pb "user_service/genproto/delivery-protos/user_service"
)

type IStorage interface {
	Close()
	User() IUserStorage
	Branch() IBranchStorage
	Client() IClientStorage
	Courier() ICourierStorage
}

type IUserStorage interface {
	Create(context.Context, *pb.CreateUser) (*pb.User, error)
	GetByID(context.Context, *pb.GetUserByID) (*pb.User, error)
	GetAll(context.Context, *pb.GetUserList) (*pb.UsersResponse, error)
	Update(context.Context, *pb.UpdateUser) (*pb.UserResponse, error)
	Delete(context.Context, *pb.GetUserByID) (*pb.UserResponse, error)
	GetPassword(context.Context, string) (string, error)
	UpdatePassword(context.Context, *pb.UpdateUserPasswordRequest) (*pb.UserResponse, error)
	GetByCredentials(context.Context, *pb.UserLoginRequest) (*pb.User, error) 
}

type IBranchStorage interface {
	Create(context.Context, *pb.CreateBranch) (*pb.Branch, error)
	GetByID(context.Context, *pb.GetBranchByID) (*pb.Branch, error)
	GetAll(context.Context, *pb.GetBranchList) (*pb.BranchesResponse, error)
	Update(context.Context, *pb.UpdateBranch) (*pb.BranchResponse, error)
	Delete(context.Context, *pb.GetBranchByID) (*pb.BranchResponse, error)
	GetActiveBranches(context.Context, *pb.GetBranchList) (*pb.BranchesResponse, error)
}

type IClientStorage interface {
	Create(context.Context, *pb.CreateClient) (*pb.Client, error)
	GetByID(context.Context, *pb.GetClientByID) (*pb.Client, error)
	GetAll(context.Context, *pb.GetClientList) (*pb.ClientsResponse, error)
	Update(context.Context, *pb.UpdateClient) (*pb.ClientResponse, error)
	Delete(context.Context, *pb.GetClientByID) (*pb.ClientResponse, error)
	UpdateClientData(context.Context, *pb.UpdateClientData) (*pb.ClientResponse, error) 
}

type ICourierStorage interface {
	Create(context.Context, *pb.CreateCourier) (*pb.Courier, error)
	GetByID(context.Context, *pb.GetCourierByID) (*pb.Courier, error)
	GetAll(context.Context, *pb.GetCourierList) (*pb.CouriersResponse, error)
	Update(context.Context, *pb.UpdateCourier) (*pb.CourierResponse, error)
	Delete(context.Context, *pb.GetCourierByID) (*pb.CourierResponse, error)
}
