CREATE TABLE IF NOT EXISTS "category" (
    "id" UUID PRIMARY KEY NOT NULL,
    "title" VARCHAR NOT NULL,
    "image" VARCHAR  NOT NULL,
    "active" VARCHAR NOT NULL,
    "category_id" UUID REFERENCES category(id),
    "created_at" TIMESTAMP DEFAULT NOW(),
    "updated_at" TIMESTAMP,
    "deleted_at" INT DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "product" (
    "id" UUID PRIMARY KEY NOT NULL,
    "title" VARCHAR NOT NULL,
    "description" VARCHAR NOT NULL, 
    "photo" VARCHAR  NOT NULL, 
    "active" VARCHAR NOT NULL,
    "type" VARCHAR CHECK ("type" IN ('modifier', 'product'))  NOT NULL,
    "price" INT NOT NULL,
    "category_id" UUID REFERENCES category(id),
    "created_at" TIMESTAMP DEFAULT NOW(),
    "updated_at" TIMESTAMP,
    "deleted_at" INT DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "order_product" (
    "id" UUID PRIMARY KEY NOT NULL,
    "product_id" UUID REFERENCES product(id),
    "quantity" INT NOT NULL,
    "price" INT NOT NULL,
    "created_at" TIMESTAMP DEFAULT NOW(),
    "updated_at" TIMESTAMP,
    "deleted_at" INT DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "client" (
    "id" UUID PRIMARY KEY NOT NULL,
    "first_name" VARCHAR NOT NULL,
    "last_name" VARCHAR NOT NULL,
    "phone" VARCHAR UNIQUE NOT NULL,
    "photo" VARCHAR  NOT NULL,
    "date_of_birth" VARCHAR NOT NULL,
    "age" INT NOT NULL,
    "gender" VARCHAR CHECK ("gender" IN ('male', 'female'))  NOT NULL,
    "last_ordered_date" VARCHAR NOT NULL,
    "total_order_sum" INT NOT NULL,
    "total_order_count" INT NOT NULL,
    "discount_type" VARCHAR CHECK ("discount_type" IN ('sum', 'percent'))  NOT NULL,
    "discount_amount" INT NOT NULL,
    "created_at" TIMESTAMP DEFAULT NOW(),
    "updated_at" TIMESTAMP,
    "deleted_at" INT DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "alternative" (
    "id" UUID PRIMARY KEY NOT NULL,
    "from_price" INT NOT NULL,
    "to_price" INT NOT NULL,
    "price" INT NOT NULL,
    "created_at" TIMESTAMP DEFAULT NOW(),
    "updated_at" TIMESTAMP,
    "deleted_at" INT DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "delivery_tarif" (
    "id" UUID PRIMARY KEY NOT NULL,
    "name" VARCHAR NOT NULL,
    "type" VARCHAR CHECK ("type" IN ('fixed', 'alternative')) NOT NULL,
    "base_price" INT NOT NULL,
    "alternative_id" UUID REFERENCES alternative(id),
    "created_at" TIMESTAMP DEFAULT NOW(),
    "updated_at" TIMESTAMP,
    "deleted_at" INT DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "branch" (
   "id" UUID PRIMARY KEY NOT NULL,
   "name" VARCHAR NOT NULL,
   "phone" VARCHAR(13) NOT NULL,
   "photo" VARCHAR NOT NULL,
   "delivery_tarif_id" UUID REFERENCES delivery_tarif(id),
   "working_hour" VARCHAR CHECK ("working_hour" IN ('start', 'end')) NOT NULL,
   "address" VARCHAR NOT NULL,
   "destination" VARCHAR NOT NULL,
   "active" VARCHAR NOT NULL,
   "created_at" TIMESTAMP DEFAULT NOW(),
   "updated_at" TIMESTAMP,
   "deleted_at" INT DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "courier" (
    "id" UUID PRIMARY KEY NOT NULL,
    "first_name" VARCHAR NOT NULL,
    "last_name" VARCHAR NOT NULL,
    "phone" VARCHAR UNIQUE NOT NULL,
    "date_of_birth" VARCHAR NOT NULL,
    "age" INT NOT NULL,
    "gender" VARCHAR NOT NULL,
    "active" VARCHAR NOT NULL,
    "login" VARCHAR NOT NULL,
    "password" VARCHAR NOT NULL,
    "max_order_count" INT NOT NULL,
    "branch_id" UUID REFERENCES branch(id),
    "created_at" TIMESTAMP DEFAULT NOW(),
    "updated_at" TIMESTAMP,
    "deleted_at" INT DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "users" (
    "id" UUID PRIMARY KEY NOT NULL,
    "first_name" VARCHAR NOT NULL,
    "last_name" VARCHAR NOT NULL,
    "phone" VARCHAR(13) NOT NULL,
    "date_of_birth" VARCHAR NOT NULL,
    "age" INT NOT NULL,
    "gender" VARCHAR NOT NULL,
    "active" VARCHAR NOT NULL,
    "login" VARCHAR UNIQUE NOT NULL,
    "password" VARCHAR NOT NULL,
    "created_at" TIMESTAMP DEFAULT NOW(),
    "updated_at" TIMESTAMP,
    "deleted_at" INT DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "orders" (
    "id" UUID PRIMARY KEY NOT NULL,
    "order_number" INT DEFAULT 123456,
    "client_id" UUID REFERENCES client(id),
    "branch_id" VARCHAR NOT NULL,
    "type" VARCHAR CHECK("type" IN ('delivery', 'pick-up')) NOT NULL,
    "address" VARCHAR NOT NULL,
    "courier_id" UUID REFERENCES courier(id),
    "price" INT NOT NULL,
    "delivery_price" INT NOT NULL,
    "discount" INT NOT NULL,
    "status" VARCHAR CHECK ("status" IN ('accepted', 'courier accepted', 'ready in branch', 'on way', 'finished', 'canceled')) NOT NULL,
    "payment_type" VARCHAR CHECK ("payment_type" IN ('card', 'cash')) NOT NULL,
    "alternative_id" UUID REFERENCES alternative(id),
    "order_product_id" UUID REFERENCES order_product(id),
    "created_at" TIMESTAMP DEFAULT NOW(),
    "updated_at" TIMESTAMP,
    "deleted_at" INT DEFAULT 0
);