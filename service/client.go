package service

import (
	"context"
	"fmt"
	"log"
	pbu "user_service/genproto/delivery-protos/user_service"
	"user_service/grpc/client"
	"user_service/storage"
)

type clientService struct {
	storage  storage.IStorage
	services client.IServiceManager
	pbu.UnimplementedClientServiceServer
}

func NewClientService(storage storage.IStorage, services client.IServiceManager) *clientService {
	return &clientService{
		storage:  storage,
		services: services,
	}
}

func (c *clientService) Create(ctx context.Context, request *pbu.CreateClient) (*pbu.Client, error) {
	client, err := c.storage.Client().Create(ctx, request)
	if err != nil {
		log.Println("error in client service in service package while creating client")
		return nil, err
	}
	return client, nil
}

func (c *clientService) GetByID(ctx context.Context, request *pbu.GetClientByID) (*pbu.Client, error) {
	client, err := c.storage.Client().GetByID(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting client by id")
		return nil, err
	}
	return client, nil
}

func (c *clientService) GetAll(ctx context.Context, request *pbu.GetClientList) (*pbu.ClientsResponse, error) {
	clients, err := c.storage.Client().GetAll(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting client list")
		return nil, err
	}
	return clients, nil
}

func (c *clientService) Update(ctx context.Context, request *pbu.UpdateClient) (*pbu.ClientResponse, error) {
	response, err := c.storage.Client().Update(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while updating client by id")
		return nil, err
	}
	return response, nil
}

func (c *clientService) Delete(ctx context.Context, request *pbu.GetClientByID) (*pbu.ClientResponse, error) {
	response, err := c.storage.Client().Delete(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while deleting client by id")
		return nil, err
	}
	return response, nil
}

func (c *clientService) UpdateFinishedOrder(ctx context.Context, request *pbu.UpdateClientData) (*pbu.ClientResponse, error) {
	fmt.Println("hello >>>>>>> ", request)
	response, err := c.storage.Client().UpdateClientData(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while updating client some data by id")
		return nil, err
	}
	return response, nil
}
