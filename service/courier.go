package service

import (
	"context"
	"fmt"
	"log"
	pbu "user_service/genproto/delivery-protos/user_service"
	"user_service/grpc/client"
	"user_service/storage"
)

type courierService struct {
	storage  storage.IStorage
	services client.IServiceManager
	pbu.UnimplementedCourierServiceServer
}

func NewCourierService(storage storage.IStorage, services client.IServiceManager) *courierService {
	return &courierService{
		storage:  storage,
		services: services,
	}
}

func (c *courierService) Create(ctx context.Context, request *pbu.CreateCourier) (*pbu.Courier, error) {
	courier, err := c.storage.Courier().Create(ctx, request)
	if err != nil {
		log.Println("error in courier service in service package while creating courier")
		return nil, err
	}
	return courier, nil
}

func (c *courierService) GetByID(ctx context.Context, request *pbu.GetCourierByID) (*pbu.Courier, error) {
	courier, err := c.storage.Courier().GetByID(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting courier by id")
		return nil, err
	}
	return courier, nil
}

func (c *courierService) GetAll(ctx context.Context, request *pbu.GetCourierList) (*pbu.CouriersResponse, error) {
	couriers, err := c.storage.Courier().GetAll(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while getting courier list")
		return nil, err
	}
	return couriers, nil
}

func (c *courierService) Update(ctx context.Context, request *pbu.UpdateCourier) (*pbu.CourierResponse, error) {
	response, err := c.storage.Courier().Update(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while updating courier by id")
		return nil, err
	}
	return response, nil
}

func (c *courierService) Delete(ctx context.Context, request *pbu.GetCourierByID) (*pbu.CourierResponse, error) {
	response, err := c.storage.Courier().Delete(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while deleting courier by id")
		return nil, err
	}
	return response, nil
}
