package service

import (
	"context"
	"errors"
	"fmt"
	"log"
	pb "user_service/genproto/delivery-protos/user_service"
	"user_service/grpc/client"
	"user_service/pkg/security"
	"user_service/storage"
)

type authService struct {
	storage  storage.IStorage
	services client.IServiceManager
	pb.UnimplementedAuthServiceServer
}

func NewAuthService(storage storage.IStorage, services client.IServiceManager) *authService {
	return &authService{
		storage:  storage,
		services: services,
	}
}

func (a *authService) UserLogin(ctx context.Context, request *pb.UserLoginRequest) (*pb.UserLoginResponse, error) {
	user, err := a.storage.User().GetByCredentials(ctx, request)
	if err != nil {
		log.Println("error while getting user by login")
		return nil, err
	}

	fmt.Println("user password", user.GetPassword(), "req password", request.GetPassword())

	if !security.CompareHashAndPassword(user.GetPassword(), request.GetPassword()) {
		fmt.Println("invalid password")
		return nil, errors.New("invalid password")
	}

	return &pb.UserLoginResponse{
		Id:    user.GetId(),
        Login: user.GetLogin(),
		}, nil
}
