package service

import (
	"context"
	"fmt"
	"log"
	pbu "user_service/genproto/delivery-protos/user_service"
	"user_service/grpc/client"
	"user_service/storage"

	"golang.org/x/crypto/bcrypt"
)

type userService struct {
	storage  storage.IStorage
	services client.IServiceManager
	pbu.UnimplementedUserServiceServer
}

func NewUserService(storage storage.IStorage, services client.IServiceManager) *userService {
	return &userService{
		storage:  storage,
		services: services,
	}
}

func (u *userService) Create(ctx context.Context, request *pbu.CreateUser) (*pbu.User, error) {

	user, err := u.storage.User().Create(ctx, request)
	if err != nil {
		log.Println("error in user service in service package while create user")
		return nil, err
	}

	return user, nil

}

func (u *userService) GetByID(ctx context.Context, request *pbu.GetUserByID) (*pbu.User, error) {

	user, err := u.storage.User().GetByID(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while get user by id")
		return nil, err
	}

	return user, nil

}

func (u *userService) GetAll(ctx context.Context, request *pbu.GetUserList) (*pbu.UsersResponse, error) {

	users, err := u.storage.User().GetAll(ctx, request)
	if err != nil {
		fmt.Println("error while in service layer get user list")
		return nil, err
	}

	return users, nil
}

func (u *userService) Update(ctx context.Context, request *pbu.UpdateUser) (*pbu.UserResponse, error) {

	response, err := u.storage.User().Update(ctx, request)
	if err != nil {
		fmt.Println("error while in service layer update user by id")
	}

	return response, nil

}

func (u *userService) Delete(ctx context.Context, request *pbu.GetUserByID) (*pbu.UserResponse, error) {

	response, err := u.storage.User().Delete(ctx, request)
	if err != nil {
		fmt.Println("error while in service layer delete user by id")
	}

	return response, nil
}

func (u *userService) UpdatePassword(ctx context.Context, request *pbu.UpdateUserPasswordRequest) (*pbu.UserResponse, error) {

	oldPassword, err := u.storage.User().GetPassword(ctx, request.Login)
	if err != nil {
		fmt.Println("ERROR in service layer while getting user password")
		return nil, err
	}
	fmt.Println(oldPassword)
	err = bcrypt.CompareHashAndPassword([]byte(oldPassword), []byte(request.OldPassword))
	fmt.Println("log123 : >>>>>>>")
	if err != nil {
		fmt.Println("old password wrong")
		return nil, err
	}

	response, err := u.storage.User().UpdatePassword(context.Background(), request)

	if err != nil {
		fmt.Println("ERROR in service layer while updating password")
		return nil, err
	}

	return response, nil
}
