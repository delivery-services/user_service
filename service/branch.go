package service

import (
	"context"
	"fmt"
	"log"
	pbu "user_service/genproto/delivery-protos/user_service"
	"user_service/grpc/client"
	"user_service/storage"
)

type branchService struct {
	storage  storage.IStorage
	services client.IServiceManager
	pbu.UnimplementedBranchServiceServer
}

func NewBranchService(storage storage.IStorage, services client.IServiceManager) *branchService {
	return &branchService{
		storage:  storage,
		services: services,
	}
}

func (b *branchService) Create(ctx context.Context, request *pbu.CreateBranch) (*pbu.Branch, error) {
	
	branch, err := b.storage.Branch().Create(ctx, request)
	
	if err != nil {
		log.Println("error in branch service in service package while creating branch", err.Error())
		return nil, err
	}

	return branch, nil
}

func (b *branchService) GetByID(ctx context.Context, request *pbu.GetBranchByID) (*pbu.Branch, error) {
	
	branch, err := b.storage.Branch().GetByID(ctx, request)
	
	if err != nil {
		fmt.Println("error in service layer while getting branch by id")
		return nil, err
	}

	return branch, nil
}

func (b *branchService) GetAll(ctx context.Context, request *pbu.GetBranchList) (*pbu.BranchesResponse, error) {
	
	branches, err := b.storage.Branch().GetAll(ctx, request)
	
	if err != nil {
	
		fmt.Println("error in service layer while getting branch list")
	
		return nil, err
	
	}
	
	return branches, nil
}

func (b *branchService) Update(ctx context.Context, request *pbu.UpdateBranch) (*pbu.BranchResponse, error) {
	response, err := b.storage.Branch().Update(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while updating branch by id")
		return nil, err
	}
	return response, nil
}

func (b *branchService) Delete(ctx context.Context, request *pbu.GetBranchByID) (*pbu.BranchResponse, error) {
	response, err := b.storage.Branch().Delete(ctx, request)
	if err != nil {
		fmt.Println("error in service layer while deleting branch by id")
		return nil, err
	}
	return response, nil
}


func (b *branchService) GetActiveBranches(ctx context.Context, request *pbu.GetBranchList) (*pbu.BranchesResponse, error) {
	
	branches, err := b.storage.Branch().GetActiveBranches(ctx, request)
	
	if err != nil {
	
		fmt.Println("error in service layer while getting active branches list")
	
		return nil, err
	
	}
	
	return branches, nil
}

